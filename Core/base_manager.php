<?php

/**
 * The home page manager
 */


require_once('Config/constants.php');
class BaseManager{

    protected $_conn;
    protected $database;
    protected $user;
    protected $password;
    protected $host = "localhost";
    private static $instance;
    public $table;




    private static function getConfigData(){
        return array(
            'DB_HOST'   => DB_HOST,
            'DB_USER'   => DB_USER,
            'DB_PWD'    => DB_PWD,
            'DB_NAME'   => DB_NAME,
        );
    }


    public static function getInstance ()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private static function initDatabase()
    {
        $database = self::getInstance();
        $config = self::getConfigData();;
        $database->_conn = new PDO("mysql:host={$config['DB_HOST']};dbname={$config['DB_NAME']}", $config['DB_USER'] , $config['DB_PWD']);
        $database->_conn->exec("set names utf8");
        return $database;
    }

    public static function getConnection()
    {
        try {
            $db = self::initDatabase();
            return $db->_conn;
        } catch (Exception $ex) {
            echo "Error Establishing Database Connection " . $ex->getMessage();
            return null;
        }
    }

    public function getOneArr($id = false , $column = 'id'){

        $dbConn = self::getConnection();

        if(!$id){
            return array();
        }

        /* Select queries return a resultset */
        $stmt = $dbConn->prepare("SELECT * FROM " . $this->table .  " WHERE " . $column . " = ?");
        $stmt->execute( [$id] );
        $data   = $stmt->fetch(PDO::FETCH_ASSOC); // fetch data

        return $data;
    }


    public function getManyArr($query = false , $params = false){

        $dbConn = self::getConnection();

        if(!$query || !$params){
            return array();
        }

        /* Select queries return a resultset */
        $stmt = $dbConn->prepare($query);
        $stmt->execute( $params );
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->close();
        return $result;
    }

    public function updateOne( $updateArr , $id, $column = 'id' ){

        $dbConn = self::getConnection();

        $query = ' UPDATE '.$this->table. ' SET ' ;
        $data = [];

        $numItems = count($updateArr);
        $i = 0;

        foreach ($updateArr as $column_key => $column_val) {
            if (++$i === $numItems) {
                $query .= $column_key . ' = ? ';
            }else{
                $query .= $column_key . ' = ?, ';
            }
            $data[] = $column_val;

        }

        $query .= ' WHERE ' . $column .  ' = ' . $id . ' ';

        $stmt = $dbConn->prepare( $query );
        return $stmt->execute( $data );
    }



}
