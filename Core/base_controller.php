<?php

/**
 * The home page controller
 */


require_once('Config/constants.php');
require_once('Core/base_manager.php');

class BaseController{
    private $controller;
    private $method;
    private $model;
    private $view;
    private $requestedParams;
    public $data = [];
    public $db_conn;
    public $user_data;

    function __construct(){
        session_start();
        $this->db_conn   = BaseManager::getInstance()::getConnection();
        $this->user_data = $this->getUserData();
    }

    function load_view($page, $data){
        if (count($data)) {
            extract($data);
        }
        require $page;
    }


    public function set_controller($controller){
        $this->controller = $controller;
    }

    public function get_controller(){
        return $this->controller;
    }

    public function set_method($method){
        $this->method = $method;
    }

    public function get_method(){
        return $this->method;
    }

    public function set_model($model){
        $this->model = $model;
    }

    public function get_model(){
        return $this->model;
    }

    public function get_model_instance( $path , $model ){
        require_once "Models/" .  $path . ".php";
        return $model::getInstance();
    }

    public function set_view($view){
        $this->view = $view;
    }

    public function get_view(){
        return $this->view;
    }

    public function set_requestedParams($requestedParams){
        $this->requestedParams = $requestedParams;
    }

    public function get_requestedParams(){
        return $this->requestedParams;
    }

    public function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function getUserData(){
        if( !isset($_SESSION['user_id']) ){
            return true;
        }

        require_once "Models/user_model.php";

        $UserModel  = new UserModel;
        $user_data  = $UserModel->getUserData($_SESSION['user_id']);

        if ($user_data){
            $_SESSION['user_data'] = $user_data;
        }

        return true;
    }


}
