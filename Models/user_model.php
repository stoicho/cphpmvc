<?php

require_once('Core/base_manager.php');

/**
 * The about page model
 */
class UserModel extends BaseManager{

    function __construct(){
        $this->table = 'users';
    }

    public function getUserData( $user_id = false ){
        if( !$user_id ){
            return [];
        }

        return parent::getOneArr($user_id);
    }

    public function checkUserForLogin( $email, $password ){
        $connection = parent::getConnection();

        /* Select queries return a resultset */
        $stmt = $connection->prepare("SELECT * FROM " . $this->table .  " WHERE email = ?  AND password = ?");
        $stmt->execute( [$email , md5($password . PW_SALT) ] );
        $data   = $stmt->fetch(PDO::FETCH_ASSOC); // fetch data

        return $data;
    }


}
