<?php

require_once('Core/base_manager.php');

/**
 * The about page model
 */
class AddressModel extends BaseManager{

    function __construct(){
        $this->table = 'address';
    }

    public function getUserAddress( $user_id = false ){
        if( !$user_id ){
            return [];
        }

        return parent::getOneArr($user_id , 'user_id');
    }

    public function updateUserAddress( $data = [] , $user_id){
        return parent::updateOne($data , $user_id , 'user_id' );
    }
    public function insertUserAddress( $data = [] ){

        $dbConn = parent::getConnection();

        $dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $dbConn->prepare("INSERT INTO address (user_id, country, city, neighborhood, address) VALUES (?, ?, ?, ?, ?)");
        $stmt->execute([
            $data['user_id'],
            $data['country'],
            $data['city'],
            $data['neighborhood'],
            $data['address'],
        ]);

        $last_id = $dbConn->lastInsertId();
        return $last_id;
    }



}
