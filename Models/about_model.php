<?php

require_once('Core/base_manager.php');

/**
 * The about page model
 */
class AboutModel extends BaseManager{

    public $table = 'about';

}
