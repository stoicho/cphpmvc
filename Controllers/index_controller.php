<?php

require_once('Core/base_controller.php');

/**
 * The home page controller
 */
class IndexController extends BaseController {


    public function index(){
        return $this->load_view("Views/home.php", $this->data);
    }

}
