<?php

require_once('Core/base_controller.php');
require_once('Models/user_model.php');

/**
* The about page controller
*/
class LoginController extends BaseController{

    public function index(){
        return $this->load_view("Views/login.php", $this->data);
    }

    public function doLogin(){

        require_once "Models/user_model.php";

        $UserModel  = new UserModel;
        $user_data  = $UserModel->checkUserForLogin( $_POST['email'] , $_POST['password'] );

        if ( empty($user_data) ) {
            header("Location: " . BASE_URL . 'login');
            exit();
        }

        if ($user_data) {
            $_SESSION['user_id'] = $user_data['id'];
            $_SESSION['user_logged'] = true;
            $_SESSION['user_data'] = $user_data;
        }

        header("Location: " . BASE_URL . 'profile');
        exit();
    }

}
