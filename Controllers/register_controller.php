<?php

require_once('Core/base_controller.php');
require_once('Models/user_model.php');

/**
* The about page controller
*/
class RegisterController extends BaseController{

    function __construct(){
        parent::__construct();
    }

    public function index(){
        return $this->load_view("Views/register.php", $this->data);
    }

    public function register(){
        $post_data  = [];
        if( empty($_POST) ){
            $_SESSION['flash_data_error'] = 'Please fill your information';
            header("Location: " . BASE_URL . 'register');
            die();
        }

        foreach ($_POST as $post_key => $post_value) {
            $post_data[$post_key] = parent::test_input( $post_value );
        }

        $post_error = $this->validatePost( $post_data );

        if( $post_error['error'] ){
            $_SESSION['flash_data_error'] = $post_error['msg'];
            header("Location: " . BASE_URL . 'register' );
            die();
        }

        $this->resgiter_user($post_data);

    }

    public function validatePost( $post_value ){

        if( !isset( $post_value['name'] ) ){
            return [
                'msg'   => 'Please fill your name',
                'error' => true,
            ];
        }

        if( !isset( $post_value['surname']) ){
            return [
                'msg'   => 'Please fill your surname',
                'error' => true,
            ];
        }

        if( !isset( $post_value['country']) ){
            return [
                'msg'   => 'Please fill your country',
                'error' => true,
            ];
        }

        if( !isset( $post_value['email']) ){
            return [
                'msg'   => 'Please fill your email',
                'error' => true,
            ];
        }

        if (!filter_var($post_value["email"] , FILTER_VALIDATE_EMAIL)) {
            return [
                'msg'   => 'Please fill your CORRECT email',
                'error' => true,
            ];
        }

        if ( !isset( $post_value['password']) ) {
            return [
                'msg'   => 'Please fill your password',
                'error' => true,
            ];
        }

        if ( !isset( $post_value['repeat_password']) ) {
            return [
                'msg'   => 'Please repeat your password',
                'error' => true,
            ];
        }

        if ( $post_value['repeat_password'] != $post_value['password'] ) {
            return [
                'msg'   => 'Password doesn\'t match',
                'error' => true,
            ];
        }

        return [
            'msg'   => '',
            'error' => false,
        ];
    }

    public function resgiter_user($post_data){


        // first check the database to make sure
        // a user does not already exist with the same username and/or email
        $user_check_query   = "SELECT * FROM users WHERE email=? LIMIT 1";
        $stmt = $this->db_conn->prepare($user_check_query);
        $stmt->execute( [$post_data['email']]);
        $user   = $stmt->fetch(); // fetch data

        if ($user) { // if user exists
            $_SESSION['flash_data_error'] = 'Email already in use';
            header("Location: " . BASE_URL . 'register');
            die();
        }


        $this->db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $this->db_conn->prepare("INSERT INTO users (name, surname, email, password) VALUES (?, ?, ?, ?)");
        $stmt->execute([
            $post_data['email'],
            $post_data['surname'],
            $post_data['email'],
            md5($post_data['password'] . PW_SALT)
        ]);


        $last_id = $this->db_conn->lastInsertId();

        $_SESSION['user_logged'] = true;
        $_SESSION['user_id']     = $last_id;
        header("Location: " . BASE_URL . 'profile');

    }



}
