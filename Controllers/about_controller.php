<?php

require_once('Core/base_controller.php');
require_once('Models/about_model.php');

/**
* The about page controller
*/
class AboutController extends BaseController{

    public function index(){
		return $this->load_view("Views/about.php", $this->data);
    }

}
