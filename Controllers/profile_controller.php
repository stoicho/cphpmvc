<?php

require_once('Core/base_controller.php');
// require_once('Models/user_model.php');

/**
* The about page controller
*/
class ProfileController extends BaseController{

    public function index(){
		return $this->load_view("Views/profile.php", $this->data);
    }

    public function updateAddress(){
        require_once "Models/address_model.php";
        $addressModel  = new AddressModel;
        $this->data['address_data']  = $addressModel->getUserAddress($_SESSION['user_data']['id']);

        return $this->load_view("Views/update_address.php", $this->data);
    }

    public function doUpdate(){
        require_once "Models/address_model.php";

        $addressModel  = new AddressModel;
        $address_data  = $addressModel->getUserAddress($_SESSION['user_data']['id']);

        if( !empty($address_data) ){
            $address_data = $addressModel->updateUserAddress( $_POST , $_SESSION['user_data']['id'] );
        }else{
            $data = $_POST;
            $data['user_id'] =  $_SESSION['user_data']['id'] ;
            $address_data = $addressModel->insertUserAddress( $data );
        }

        $this->updateAddress();
    }

    public function logout(){
        unset($_SESSION['user_logged']);
        unset($_SESSION['user_id']);
        unset($_SESSION['user_data']);
        header('Location: /');
    }

}
