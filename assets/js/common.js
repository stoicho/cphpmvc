$(() => {


    let sliderClip = $('.slider__clip .owl-carousel').owlCarousel({
        loop: true,
        center: true,
        items: 1,
        margin: 0,
        dots: false,
        autoplay: true,
        smartSpeed: 450
    });


    // Slider left and right
    $('.slider__navigation .slider__next').on('click', function (e) {
        e.preventDefault();
        sliderClip.trigger('next.owl.carousel');
        sliderClip.trigger('stop.owl.autoplay');
    });

    $('.slider__navigation .slider__prev').on('click', function (e) {
        e.preventDefault();
        sliderClip.trigger('prev.owl.carousel');
        sliderClip.trigger('stop.owl.autoplay');
    });


}); // end doc ready
