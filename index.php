<?php
$url = isset($_SERVER['REQUEST_URI']) ? explode('/', ltrim($_SERVER['REQUEST_URI'], '/')) : '/';
require_once __DIR__ . '/Core/base_controller.php';
if (empty($url) || $url[0] == '') {
    // This is the home page
    // Initiate the home controller
    // and render the home view


    require_once __DIR__ . '/controllers/index_controller.php';
    $baseController    = new IndexController();
    $baseController->set_controller('index');
    $baseController->set_method('index');
    $baseController->set_model('index');
    $baseController->set_view('index');
    $baseController->set_requestedParams('index');


    $requestedAction = isset($url[1]) ? $url[1] : 'index';
    $baseController->$requestedAction();

    // print $indexView->index();
} else {


    // This is not home page
    // Initiate the appropriate controller
    // and render the required view

    //The first element should be a controller
    $requestedController = $url[0];

    // If a second part is added in the URI,
    // it should be a method
    $requestedAction = isset($url[1]) ? $url[1] : 'index';

    // The remain parts are considered as
    // arguments of the method
    $requestedParams = array_slice($url, 2);

    // Check if controller exists. NB:
    // You have to do that for the model and the view too
    $controller        = $requestedController . 'Controller';
    if (!file_exists(__DIR__ . '/controllers/' . $requestedController . '_controller.php')){
        header('HTTP/1.1 404 Not Found');
        exit('404');
    }

    require_once __DIR__ . '/controllers/' . $requestedController . '_controller.php';

    $baseController    = new $controller();
    $baseController->set_controller($requestedController);
    $baseController->set_method($requestedAction);
    $baseController->set_model($requestedController . '_model');
    $baseController->set_view($requestedController . '_view');
    $baseController->set_requestedParams($requestedParams);
    $baseController->$requestedAction();

}
