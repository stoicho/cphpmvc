const {
	src,
	dest,
	watch,
	series,
	parallel
} = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const cleancss = require('gulp-clean-css');
const del = require('del');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-terser');

const jsDestDir = 'assets/js';
const cssDestDir = 'assets/css';



function cssSite(done) { 
	src('assets/scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass())
		.on("error", sass.logError)
		.pipe(autoprefixer('last 4 versions'))
		.pipe(cleancss())
		.pipe(sourcemaps.write('./maps'))
		.pipe(dest(cssDestDir))
	done();
}

function cssMinifySite( done ) { 
	src([
			'assets/css/owl.carousel.css',
			'assets/css/owl.theme.default.css',
        ])
		.pipe(cleancss())
        .pipe(concat('build.css'))
        .pipe(dest(cssDestDir));
	done();
}



function buildLibs(done) {
	src([
            'assets/js/libs/owl.carousel.min.js',
		])
		.pipe(concat('buildLibs.js'))
		.pipe(uglify({
            compress: {
                drop_console: true
            }
		}))
		// .pipe(sourcemaps.write('.'))
		.pipe(dest(jsDestDir))
	done();
}

function buildSite(done) {

	src([
			'assets/js/common.js',
		])
		.pipe(concat('build.js'))
		.pipe(uglify({
            compress: {
                drop_console: true
            }
		}))
		// .pipe(sourcemaps.write('.'))
		.pipe(dest(jsDestDir))
	done();
}

function watchFiles() {
	watch('assets/scss/**/*.scss', series(cssSite, reload));
}

function reload(done) {
	done();
}


exports.watch = parallel(cssSite, watchFiles); // sync
exports.buildSite = series(parallel(cssMinifySite, buildSite, buildLibs));