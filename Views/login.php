<?php include 'views/head.php' ?>

<section class="section">
	<div class="shell">
		<h1 class="page__title text__center text__uppercase">
			Login Page
		</h1><!-- /.page__title .text__center .text__uppercase -->



		<div class="block__container">
			<form action="/login/doLogin" method="POST" class="login__form">

				<div class="form-row">

					<div class="form-group col-lg-6 col-md-6">
						<label for="email" class="base__label">
							Email
						</label>
						<input type="email" name="email" id="email" class="form-control field" title="Email" placeholder="Email" required>
					</div><!-- /.form-group .col-lg-6 .col-md-6 -->

					<div class="form-group col-lg-6 col-md-6">
						<label for="password" class="base__label">
							Password
						</label>
						<input type="password" name="password" id="password" class="form-control field" title="Password" placeholder="Password" required>
					</div><!-- /.form-group .col-lg-6 .col-md-6 -->

				</div>


				<div class="page__actions">
					<button type="submit" class="login_event btn__common">
						Log in
					</button><!-- /.login_event -->
				</div><!-- /.page__actions -->
			</form><!-- /.login__form -->

		</div><!-- /.block__container -->


	</div><!-- /.shell -->
</section><!-- /.section -->




<?php include 'views/footer.php' ?>
