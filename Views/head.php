<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title>Shop CMS</title>

	<link rel="stylesheet" href="/assets/css/all.min.css">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/owl.carousel.css">
	<link rel="stylesheet" href="/assets/css/owl.theme.default.css">
	<link rel="stylesheet" href="/assets/css/style.css">
</head>

<body>
	<header class="header">
		<div class="shell">
			<div class="header__container">

				<nav class="main__top__navigation">
					<ul class="main__navigation">
						<li>
							<a href="/index">
								Home
							</a>
						</li>
						<li>
							<a href="/about">
								About
							</a>
						</li>
					</ul><!-- /.main__navigation -->
				</nav><!-- /.main__top__navigation -->

				<nav class="user__navigation">
					<ul class="main__user__nav">
                        <?php if(!isset($_SESSION['user_logged'])) :?>
                            <li>
                                <a href="/login">
                                    Login
                                </a>
                            </li>
                            <li>
                                <a href="/register">
                                    Registration
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="/profile">
                                    Profile
                                </a>
                            </li>

                            <li>
                                <a href="/profile/updateAddress">
                                    Change Address
                                </a>
                            </li>

                            <li>
                                <a href="/profile/logout">
                                    Log out
                                </a>
                            </li>
                        <?php endif ?>
					</ul><!-- /.main__user__nav -->
				</nav><!-- /.user__navigation -->
			</div><!-- /.header__container -->
		</div><!-- /.shell -->
	</header><!-- /.header -->
