<?php include 'views/head.php' ?>


<?php 
	/*
		echo $variable; 
		echo $variable2;
		echo $variable3;
		echo '<pre>' . print_r($variable4, true) . '</pre>';
	*/
?>



<section class="section">
	<div class="shell">
		<h1 class="page__title text__center text__uppercase">
			About
		</h1><!-- /.page__title .text__center .text__uppercase -->

		<div class="page__text">
			<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
				standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make 
				a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
				remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing 
				Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
			</p>
		</div><!-- /.page__text -->

		<div class="block__container">
			<form action="" method="POST" class="about__form">
		
				<div class="form-row">			
					<div class="form-group col-lg-4 col-md-4">
						<label for="name" class="base__label">
							Name	
						</label>
						<input type="text" name="name" id="name" class="form-control field" title="Name" placeholder="Name" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->	
								
					<div class="form-group col-lg-4 col-md-4">
						<label for="email" class="base__label">
							Email								
						</label>
						<input type="email" name="email" id="email" class="form-control field" title="Email" placeholder="Email" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->
	
					<div class="form-group col-lg-12 col-md-12">
						<label for="message" class="base__label">
							Message							
						</label>
						<textarea name="message" id="message" class="field__textarea"></textarea><!-- /#message -->
					</div><!-- /.form-group .col-lg-12 .col-md-12 -->
				</div>

	
	
				<div class="page__actions">
					<button type="submit" class="about__submit_event btn__common">
						Submit
					</button><!-- /.about__submit_event -->
				</div><!-- /.page__actions -->
	
	
			</form><!-- /.about__form -->
		</div><!-- /.block__container -->


	</div><!-- /.shell -->
</section><!-- /.section -->





<?php include 'views/footer.php' ?>