<footer class="footer">
	<div class="shell">
		<div class="copyrights__text text__center">
			<p>
				<?php echo date("Y"); ?>  &copy; All rights reserved.
			</p>
		</div><!-- /.copyrights__text .text__center -->		
	</div><!-- /.shell -->
</footer><!-- /.footer -->

<script src="/assets/js/jquery-3.4.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/owl.carousel.min.js"></script>
<script src="/assets/js/common.js"></script>

</body>

</html>