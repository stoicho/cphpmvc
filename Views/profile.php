<?php include 'views/head.php' ?>


<section class="section">
	<div class="shell">
		<h1 class="page__title text__center text__uppercase">
			My Profile
		</h1><!-- /.page__title .text__center .text__uppercase -->


		<div class="block__container">
			<form action="" method="POST" class="profile__form">

				<div class="form-row">
					<div class="form-group col-lg-4 col-md-4">
						<label for="name" class="base__label base__label--regular">
							Name
						</label>
						<input type="text" name="name" id="name" class="form-control field field--transparent" title="Name" value="Name" placeholder="Name" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->
					<div class="form-group col-lg-4 col-md-4">
						<label for="surname" class="base__label base__label--regular">
							Surname
						</label>
						<input type="text" name="surname" id="surname" class="form-control field field--transparent" title="Surname" value="Surname" placeholder="Surname" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->
					<div class="form-group col-lg-4 col-md-4">
						<label for="country" class="base__label base__label--regular">
							Country
						</label>
						<input type="text" name="country" id="country" class="form-control field field--transparent" title="Country" value="Country" placeholder="Country" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->
					<div class="form-group col-lg-4 col-md-4">
						<label for="email" class="base__label base__label--regular">
							Email
						</label>
						<input type="email" name="email" id="email" class="form-control field field--transparent" title="Email" value="Email" placeholder="Email" required>
					</div><!-- /.form-group .col-lg-4 .col-md-4 -->


				</div>


			</form><!-- /.profile__form -->
		</div><!-- /.block__container -->


	</div><!-- /.shell -->
</section><!-- /.section -->





<?php include 'views/footer.php' ?>
