<?php include 'views/head.php' ?>

<section class="section">


	<div class="slider__intro">
		<div class="slider__clip">
			<div class="owl-carousel owl-theme">

				<div class="item">
					<div class="slider__inner__holder">
						<a href="javascript:;" class="slider__settings slider__image__1"></a>
					</div>
				</div>

				<div class="item">
					<div class="slider__inner__holder">
						<a href="javascript:;" class="slider__settings slider__image__2"></a>
					</div>
				</div>			

				<div class="item">
					<div class="slider__inner__holder">
						<a href="javascript:;" class="slider__settings slider__image__3"></a>
					</div>
				</div>		
			
			</div><!-- /.owl-carousel owl-theme -->
			


			<div class="slider__navigation">
				<a href="javascript:;" class="slider__prev slider_action btn btn__circle btn__circle--rounded">
					<i class="fas fa-angle-left"></i>
				</a>
				<a href="javascript:;" class="slider__next slider_action btn btn__circle btn__circle--rounded">
					<i class="fas fa-angle-right"></i>
				</a>
			</div><!-- .slider__navigation -->
		
		</div><!-- /.slider__clip -->
	</div><!-- /.slider__intro -->


	<div class="shell">
		<div class="products__wrapper">
			<ul class="products__list">
				<li>
					<div class="product__item">
						<header class="product__item__header">
							<a href="javascript:;" class="product_event">
								<figure class="product__item__image text__center">
									<img src="https://via.placeholder.com/300x150" alt="">
								</figure><!-- /.product__item__image -->
							</a><!-- /.product_event -->
						</header><!-- /.product__item__header -->
						<div class="product__item__body">
							<h3 class="product__item__title text__center text__uppercase">
								<p>
									Salad
								</p>
							</h3><!-- /.product__item__title -->
							<div class="product__item__text">
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
									Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
								</p>
							</div><!-- /.product__item__text -->
							<div class="product__item__price">
								<p>
									5.45 euro
								</p>
							</div><!-- /.product__item__price -->

						</div><!-- /.product__item__body -->
						<footer class="product__item__footer">
							<a href="javascript:;">
								Buy it
							</a>
						</footer><!-- /.product__item__footer -->
							
					
					
					</div><!-- /.product__item -->
				</li>
				<li>
					<div class="product__item">
						<header class="product__item__header">
							<a href="javascript:;" class="product_event">
								<figure class="product__item__image text__center">
									<img src="https://via.placeholder.com/300x150" alt="">
								</figure><!-- /.product__item__image -->
							</a><!-- /.product_event -->
						</header><!-- /.product__item__header -->
						<div class="product__item__body">
							<h3 class="product__item__title text__center text__uppercase">
								<p>
									Salad
								</p>
							</h3><!-- /.product__item__title -->
							<div class="product__item__text">
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
									Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
								</p>
							</div><!-- /.product__item__text -->
							<div class="product__item__price">
								<p>
									5.45 euro
								</p>
							</div><!-- /.product__item__price -->

						</div><!-- /.product__item__body -->
						<footer class="product__item__footer">
							<a href="javascript:;">
								Buy it
							</a>
						</footer><!-- /.product__item__footer -->
							
					
					
					</div><!-- /.product__item -->
				</li>
				<li>
					<div class="product__item">
						<header class="product__item__header">
							<a href="javascript:;" class="product_event">
								<figure class="product__item__image text__center">
									<img src="https://via.placeholder.com/300x150" alt="">
								</figure><!-- /.product__item__image -->
							</a><!-- /.product_event -->
						</header><!-- /.product__item__header -->
						<div class="product__item__body">
							<h3 class="product__item__title text__center text__uppercase">
								<p>
									Salad
								</p>
							</h3><!-- /.product__item__title -->
							<div class="product__item__text">
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
									Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
								</p>
							</div><!-- /.product__item__text -->
							<div class="product__item__price">
								<p>
									5.45 euro
								</p>
							</div><!-- /.product__item__price -->

						</div><!-- /.product__item__body -->
						<footer class="product__item__footer">
							<a href="javascript:;">
								Buy it
							</a>
						</footer><!-- /.product__item__footer -->
							
					
					
					</div><!-- /.product__item -->
				</li>
				<li>
					<div class="product__item">
						<header class="product__item__header">
							<a href="javascript:;" class="product_event">
								<figure class="product__item__image text__center">
									<img src="https://via.placeholder.com/300x150" alt="">
								</figure><!-- /.product__item__image -->
							</a><!-- /.product_event -->
						</header><!-- /.product__item__header -->
						<div class="product__item__body">
							<h3 class="product__item__title text__center text__uppercase">
								<p>
									Salad
								</p>
							</h3><!-- /.product__item__title -->
							<div class="product__item__text">
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
									Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
								</p>
							</div><!-- /.product__item__text -->
							<div class="product__item__price">
								<p>
									5.45 euro
								</p>
							</div><!-- /.product__item__price -->

						</div><!-- /.product__item__body -->
						<footer class="product__item__footer">
							<a href="javascript:;">
								Buy it
							</a>
						</footer><!-- /.product__item__footer -->
							
					
					
					</div><!-- /.product__item -->
				</li>
			</ul><!-- /.products__list -->
		</div><!-- /.products__wrapper -->
	</div><!-- /.shell -->

</section><!-- /.section -->



<?php include 'views/footer.php' ?>
