<?php include 'views/head.php' ?>



<section class="section">
    <div class="shell">
        <h1 class="page__title text__center text__uppercase">
            Register
        </h1><!-- /.page__title .text__center .text__uppercase -->



        <?php if (isset($_SESSION['flash_data_error'])) : ?>
            <h3>
                ERROR
                <?= $_SESSION['flash_data_error'] ?>
                <!-- TODO KRISKA  -->
            </h3>
            <?php unset($_SESSION['flash_data_error']) ?>
        <?php endif ?>

        <div class="block__container">
            <form action="/profile/doUpdate" method="POST" class="register__form">

                <div class="form-row">
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="country" class="base__label">
                            country
                        </label>
                        <input type="text" name="country" id="country" class="form-control field" title="country" placeholder="country" value="<?= isset($address_data['country']) ? $address_data['country'] : '' ?>" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="city" class="base__label">
                            city
                        </label>
                        <input type="text" name="city" id="city" class="form-control field" title="city" placeholder="city" value="<?= isset($address_data['city']) ? $address_data['city'] : '' ?>" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="neighborhood" class="base__label">
                            neighborhood
                        </label>
                        <input type="text" name="neighborhood" id="neighborhood" class="form-control field" title="neighborhood" placeholder="neighborhood" value="<?= isset($address_data['neighborhood']) ? $address_data['neighborhood'] : '' ?>" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="address" class="base__label">
                            address
                        </label>
                        <input type="text" name="address" id="address" class="form-control field" title="address" placeholder="address" value="<?= isset($address_data['address']) ? $address_data['address'] : '' ?>" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                </div>


                <div class="page__actions">
                    <button type="submit" class="register_event btn__common">
                        Update
                    </button><!-- /.register_event -->
                </div><!-- /.page__actions -->


            </form><!-- /.register__form -->
        </div><!-- /.block__container -->


    </div><!-- /.shell -->
</section><!-- /.section -->





<?php include 'views/footer.php' ?>
