<?php include 'views/head.php' ?>




<section class="section">
    <div class="shell">
        <h1 class="page__title text__center text__uppercase">
            Register
        </h1><!-- /.page__title .text__center .text__uppercase -->



        <?php if (isset($_SESSION['flash_data_error'])) : ?>
            <h3>
                ERROR
                <?= $_SESSION['flash_data_error'] ?>
                <!-- TODO KRISKA  -->
            </h3>
            <?php unset($_SESSION['flash_data_error']) ?>
        <?php endif ?>

        <div class="block__container">
            <form action="/register/register" method="POST" class="register__form">

                <div class="form-row">
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="name" class="base__label">
                            Name
                        </label>
                        <input type="text" name="name" id="name" class="form-control field" title="Name" placeholder="Name" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="surname" class="base__label">
                            Surname
                        </label>
                        <input type="text" name="surname" id="surname" class="form-control field" title="Surname" placeholder="Surname" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="country" class="base__label">
                            Country
                        </label>
                        <input type="text" name="country" id="country" class="form-control field" title="Country" placeholder="Country" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="email" class="base__label">
                            Email
                        </label>
                        <input type="email" name="email" id="email" class="form-control field" title="Email" placeholder="Email" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->

                    <div class="form-group col-lg-4 col-md-4">
                        <label for="password" class="base__label">
                            Password
                        </label>
                        <input type="password" name="password" id="password" class="form-control field" title="Password" placeholder="Password" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->

                    <div class="form-group col-lg-4 col-md-4">
                        <label for="repeat_password" class="base__label">
                            Repeat Password
                        </label>
                        <input type="password" name="repeat_password" id="repeat_password" class="form-control field" title="Repeat Password" placeholder="Repeat Password" required>
                    </div><!-- /.form-group .col-lg-4 .col-md-4 -->

                </div>


                <div class="page__actions">
                    <button type="submit" class="register_event btn__common">
                        Register
                    </button><!-- /.register_event -->
                </div><!-- /.page__actions -->


            </form><!-- /.register__form -->
        </div><!-- /.block__container -->


    </div><!-- /.shell -->
</section><!-- /.section -->





<?php include 'views/footer.php' ?>
